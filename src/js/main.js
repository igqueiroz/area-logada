$(document).ready(function() {
  if ($("#inscricoes, .matricula-documento, #matricula")[0]) {
    // função para rodar interna a div
    var element = $(".red-line");
    var speed = 15,
      scroll = 5,
      scrolling;

    $(".arrow-function-up").mouseenter(function() {
      detectArrowUp();
      // Scroll the element up
      scrolling = window.setInterval(function() {
        element.scrollTop(element.scrollTop() - scroll);
      }, speed);
    });

    $(".arrow-function-down").mouseenter(function() {
      detectArrowUp();
      // Scroll the element down
      scrolling = window.setInterval(function() {
        element.scrollTop(element.scrollTop() + scroll);
      }, speed);
    });

    $(".arrow-function-up, .arrow-function-down").bind({
      click: function(e) {
        // Prevent the default click action
        e.preventDefault();
      },
      mouseleave: function() {
        if (scrolling) {
          window.clearInterval(scrolling);
          scrolling = false;
        }
        detectArrowUp();
      }
    });

    $(".red-line").scroll(function() {
      if (element.scrollTop() === 0) {
        $(".arrow-function-up")
          .css("display", "none")
          .fadeOut();
      } else {
        $(".arrow-function-up")
          .css("display", "flex")
          .fadeIn();
      }
    });

    function detectArrowUp() {
      if (element.scrollTop() === 0) {
        $(".arrow-function-up").css("display", "none");
      } else {
        $(".arrow-function-up").css("display", "flex");
      }
    }

    // Detect mobile devices e Fetch para Mobile
    var isMobile = window.matchMedia("only screen and (max-width: 760px)")
      .matches;

    if (isMobile) {
      // Carregamento somente no Mobile

      // Corrige o menu para evitar o fechamento na área logada
      $(".perfil-nav li, .change-pic").click(function(event) {
        event.stopPropagation();
      });

      // Função para fazer o sticky header no texto Minhas Inscrições:
      window.onscroll = function() {
        scrollingFixed();
      };
      var header = $("#title");
      var sticky = $("#title").offset() ? $("#title").offset().top : undefined;

      function scrollingFixed() {
        if (sticky !== undefined) {
          if (window.pageYOffset > sticky) {
            header.addClass("sticky");
          } else {
            header.removeClass("sticky");
          }
        }
      }

      $(".list-inscricaoes li .open-box-mobile").click(function(e) {
        var el = $(this).parent();
        $(".list-inscricaoes li").removeClass("active");

        // Exemplo cada vez que o usuário coloca a seta para baixo, também faz um fetch para atualização dos status dos boxes
        if (!el.find(".open-box-mobile").hasClass("active")) {
          el.find(".open-box-mobile").toggleClass("active");
          el.find(".mobile-info").empty();
          var getStatus = el.attr("class");
          var type = el.closest("[data-type]").data("type");
          fetch(`/${type}-conteudo-${getStatus}.html`)
            .then(function(result) {
              return result.text();
            })
            .then(function(data) {
              el.find(".mobile-info")
                .append(data)
                .fadeIn();
            })
            .catch(function(error) {
              console.log("Erro: " + error.message);
            });
        } else {
          el.find(".mobile-info").slideToggle("slow");
          el.find(".open-box-mobile").toggleClass("active");
        }
      });
    } else {
      // Carregamento somente no Desktop

      // Fetch para Desktop
      $(".list-inscricaoes li").click(function(e) {
        $(".list-inscricaoes li").removeClass("active");
        var getStatus = $(this).attr("class");
        var type = $(this)
          .closest("[data-type]")
          .data("type");

        // reseta todos os ícones
        $(".list-inscricaoes li").each(function(ind) {
          var itemSrc = $(".list-inscricaoes li")
            .eq(ind)
            .find("img")
            .attr("src");
          if (itemSrc.indexOf("white") > 0) {
            var iconColor = itemSrc.split("-white.svg");
            $(".list-inscricaoes li")
              .eq(ind)
              .find("img")
              .attr("src", function(i, val) {
                return iconColor[0] + ".svg";
              });
          }
        });

        // troca a cor do atual
        var iconColorAtual = $(this)
          .find("img")
          .attr("src")
          .split(".svg");
        $(this)
          .find("img")
          .attr("src", function(i, val) {
            return iconColorAtual[0] + "-white.svg";
          });
        $(this).addClass("active");

        // exemplo que carrega novo arquivo no miolo
        $("#info-inscricoes").empty();
        fetch(`/${type}-conteudo-${getStatus}.html`)
          .then(function(result) {
            return result.text();
          })
          .then(function(data) {
            $("#info-inscricoes")
              .append(data)
              .fadeIn();
          })
          .catch(function(error) {
            console.log("Erro: " + error.message);
          });
      });
    }
  }
  //Js Matricula
  if ($("#matricula")[0]) {
    var elementToFetch = $(".elementToFetch");
    $('[name="razao-social"]').change(function() {
      elementToFetch.empty();
      var elementId = $(this).attr("id");
      fetch("/matricula-conteudo-" + elementId + ".html")
        .then(function(result) {
          return result.text();
        })
        .then(function(data) {
          elementToFetch.append(data);
          fetchResponsavelFinanceiro();
        })
        .catch(function(error) {
          console.log("Erro: " + error.message);
        });
    });

    function fetchResponsavelFinanceiro() {
      var elementresponsavel = $(".responsavel");
      $('[name="responsavelFinanceiro"]').change(function() {
        elementresponsavel.empty();
        var elementId = $(this).attr("id");
        elementId =
          elementId === "responsavelFinanceiroSim"
            ? "responsavel-financeiro-sim"
            : "responsavel-financeiro-nao";
        fetch("/matricula-conteudo-" + elementId + ".html")
          .then(function(result) {
            return result.text();
          })
          .then(function(data) {
            elementresponsavel.append(data);
          })
          .catch(function(error) {
            console.log("Erro: " + error.message);
          });
      });
    }
  }

  // Inicializa o Popover do Menu e Botões de senha

  if ($(".notifications")[0]) {
    // Popover dos Dados Pessoais
    $('[data-toggle="popover"]').popover({
      html: true,
      template: `<div class="popover bs-popover-right" role="tooltip">
          <div class="arrow" x-arrow></div>
          <div class="popover-content"></div>
          </div>`,
      trigger: "click"
    });

    $('[data-toggle="popover2"]').popover({
      html: true,
      template: `<div class="popover bs-popover-right2 popover2" role="tooltip">
          <div class="arrow" x-arrow></div>
          <div class="popover-content"></div>
          </div>`,
      trigger: "click"
    });
  }
});

// Fetch para popular os dados de perfil no menu
function menuPerfil(element) {
  var dataType = element.data("type");
  var dataBody = $("nav #dados-wrapper");
  $(".perfil-nav li").removeClass("active");
  element.addClass("active");
  dataBody.empty();
  fetch(`../${dataType}.html`)
    .then(function(result) {
      return result.text();
    })
    .then(function(data) {
      dataBody.append(data).fadeIn();
    })
    .catch(function(error) {
      console.log("Erro: " + error.message);
    });
}

// Remove o bug dos Popovers abrindo um por cima do outro
function clearPopovers(btnClick, element) {
  // se algum botão dentro do popover for clicado o popover desaparece
  if (btnClick) {
    $('[data-toggle="popover"]').popover("hide");
    $(".popover").hide();
    $('[data-toggle="popover2"]').popover("hide");
    $(".popover2").hide();
  } else {
    var hideAnotherPopover =
      $(element).data("toggle") === "popover" ? "popover2" : "popover";
    $('[data-toggle="' + hideAnotherPopover + '"]').popover("hide");
    $("." + hideAnotherPopover + "").hide();
  }
}

// corrige o funcionamento dos selects
function selectedOption(me) {
  if ($(me).val() === "") {
    $(me).removeClass("selected");
  } else {
    $(me).addClass("selected");
  }
}

function formasPagamento(element) {
  $(".formas-pagamento").empty();
  fetch(`/formas-pagamento.html`)
    .then(function(result) {
      return result.text();
    })
    .then(function(data) {
      $(".formas-pagamento").append(data);
    })
    .catch(function(error) {
      console.log("Erro: " + error.message);
    });
}

function cartaoCredito(element) {
  $(element).addClass("selected");
  fetch(`/cartao-credito.html`)
    .then(function(result) {
      return result.text();
    })
    .then(function(data) {
      $(".formas-pagamento").append(data);
    })
    .catch(function(error) {
      console.log("Erro: " + error.message);
    });
}

function openIframePagamento(element) {
  $(".btn-cartao").removeClass("selected");
  $(element).addClass("selected");
  $(".iframe-pagamento").empty();
  $(".iframe-pagamento").append(
    '<div class="iframe">IFRAME DE PAGAMENTO</div>'
  );
}
