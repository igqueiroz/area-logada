// gulpfile.js
'use strict'

// Add our dependencies
var gulp = require('gulp'), // Main Gulp module
    concat = require('gulp-concat'), // Gulp File concatenation plugin
    open = require('gulp-open'), // Gulp browser opening plugin
    connect = require('gulp-connect'), // Gulp Web server runner plugin
    fileinclude = require('gulp-file-include'), // Gulp Include files
    markdown = require('markdown'),
    imagemin = require('gulp-imagemin'),  // Minify images
    sass = require('gulp-sass'),  // Gulp SASS
    autoprefixer = require('gulp-autoprefixer'),  // Autoprefixer for CSS
    sourcemaps = require('gulp-sourcemaps')  // Make Sourcemaps
    
    sass.compiler = require('node-sass')

// Configuration
var configuration = {
    paths: {
        src: {
            partials: [
            	'./src/*.html', 
            	'./src/**/*.html'
            ],
            html: [
            	'./src/*.html'
            ],
            css: [
                './src/css/main.scss'
            ],
            csswatch: [
                './src/css/**/**'
            ],
            fonts: [
                './src/fonts/**'
            ],
            js: [
	        	'src/js/vendor/jquery.min.js',
	        	'src/js/vendor/jquery.validate.min.js',
	        	'src/js/vendor/bootstrap.min.js',
	        	'src/js/vendor/bootstrap-dialog.min.js',
	        	'src/js/main.js'
            ],
            img: [
            	'./src/img/**'
            ],
            emails: [
                './src/emails/**'
            ]
        },
        dist: './build'
    },
    localServer: {
        port: 8001,
        url: 'http://localhost:8001/'
    }
};

// Gulp task to concatenate our css files
gulp.task('sass', function () {
    gulp.src(configuration.paths.src.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(concat('main.css'))
        .pipe(gulp.dest(configuration.paths.dist + '/css'))
        .pipe(connect.reload())
    
    gulp.src(configuration.paths.src.fonts)
        .pipe(gulp.dest(configuration.paths.dist + '/fonts/bootstrap'))
    
    gulp.src(configuration.paths.src.emails)
        .pipe(gulp.dest(configuration.paths.dist + '/emails'))
});

// Gulp task to concatenate our js files
gulp.task('js', function () {
   gulp.src(configuration.paths.src.js)
       .pipe(concat('main.js'))
       .pipe(gulp.dest(configuration.paths.dist + '/js'))
       .pipe(connect.reload())
});

// Gulp task to create a web server
gulp.task('connect', function () {
    connect.server({
        root: configuration.paths.dist,
        port: configuration.localServer.port,
        livereload: true
    });
});

// Gulp task to join and glue HTML
gulp.task('fileinclude', function() {
  gulp.src(configuration.paths.src.html)
    .pipe(fileinclude({
		prefix: '@@',
		basepath: '@file'
    }))
    .pipe(gulp.dest(configuration.paths.dist))
    .pipe(connect.reload());
});

// Gulp task to minify images
gulp.task('imgs', function() {
    gulp.src(configuration.paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(configuration.paths.dist + '/img'))
        .pipe(connect.reload())
});

// Gulp task to open the default web browser
gulp.task('open', function(){
    gulp.src(configuration.paths.dist + '/login.html')
        .pipe(open({uri: configuration.localServer.url}));
});

// Watch the file system and reload the website automatically
gulp.task('watch', function () {
	gulp.watch(configuration.paths.src.partials, ['fileinclude']);
    gulp.watch(configuration.paths.src.html, ['fileinclude']);
    gulp.watch(configuration.paths.src.img, ['imgs']);
    gulp.watch(configuration.paths.src.csswatch, ['sass']);
    gulp.watch(configuration.paths.src.js, ['js']);
});

// Gulp tasks
gulp.task('default', ['fileinclude', 'js', 'sass', 'imgs', 'connect', 'open', 'watch' ]);
gulp.task('build', ['fileinclude', 'js', 'sass', 'imgs' ]);