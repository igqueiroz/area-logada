# Matrícula e Inscrições HTML e CSS

---

## Workflow

---

1. Para utilizar o repositório, em primeiro lugar, vamos utilizar duas nomenclaturas: o repositório upstream é o repositório da empresa, criado e mantido dentro da empresa; o repositório origin é o repositório em sua conta local;
2. A ideia é que você faça os desenvolvimentos no repositório origin e faça pull requests para o repositório upstream;

## Estrutura de Arquivos

---

```
Áreas Comuns
├── header.html (não logado)
├── header-interno.html (logado)
│   ├── notifications.html
│   ├── perfil.html
│   ├── dados-perfil.html
│   │   ├── modal-perfil-alterar-senha-1.html
│   │   ├── modal-perfil-alterar-senha-2.html
│   │   ├── modal-alterar-foto-selecionar-arquivo.html
│   │   └── modal-alterar-foto.html
│   └── dados-endereco.html
├── footer.html
├── main.css
└── main.js

Templates
├── login.html
│   ├── modal-login-recuperacao-1.html
│   ├── modal-login-recuperacao-2.html
│   └── modal-login-recuperacao-3.html
├── inscricoes.html
│   ├── inscricoes-lateral.html
│   └── inscricoes-conteudo-taxa.html
├── matricula.html (início e dados)
│   ├── header-matricula.html
│   ├── matricula-conteudo-pessoa-fisica.html
│   │   ├── matricula-conteudo-responsavel-financeiro-sim.html
│   │   └── matricula-conteudo-responsavel-financeiro-nao.html
│   └── matricula-conteudo-pessoa-juridica.html
├── matricula-contrato.html
│   └── header-matricula.html
├── matricula-documento.html
│   ├── header-matricula.html
│   ├── matricula-lateral.html
│   ├── matricula-conteudo-cpf.html
│   │   └── modal-anexar-arquivos.html (Usando a câmera se for mobile)
│   ├── matricula-conteudo-curriculo.html
│   ├── matricula-conteudo-historico-escolar.html
│   ├── matricula-com-outras-opcoes-e-modal.html
│   │   └── modal-outras-opcoes.html
│   ├── matricula-conteudo-rg.html
│   ├── matricula-conteudo-comprovante-residencia.html
│   ├── matricula-conclusao-pos-mestrado-sem-desconto.html
│   ├── matricula-conclusao-pos-mestrado-com-desconto.html
│   ├── formas-pagamento.html
│   └── cartao-credito.html (aqui abre o iframe de pagamento)
├── conclusao.html
├── pagamento-sucesso.html
├── pagamento-erro.html
└── emails
    └── recuperar-senha.html
```

## Ambiente de Deploy

### Setando seu ambiente de desenvolvimento:

Siga os passos abaixo para configurar seu ambiente de git:

1. Acesse o repositório dentro da página do Bitbucket. O repositório encontra-se dentro do projeto Admission;

2. Faça um fork do repositório. Isso irá gerar uma cópia do repositório em sua conta do bitbucket. Para ver como fazer um fork a partir de um repositório existente, [clique aqui](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html);

3. Vá para a página do seu repositório em nuvem e clone o seu repositório para sua máquina. Para ver como fazer o clone de um repositório, [clique aqui](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html);

### Mantendo seu ambiente em sincronia com o repositório upstream.

Seu ambiente já está quase setado. Falta um único detalhe: você precisa ser capaz de sincronizar seu ambiente local com as alterações dos demais desenvolvedores. Para isso, vamos utilizar o repositório upstream. Nos eu navegador de internet vá até a página do repositório upstream. Copie o endereço dele e, em seguida, em seu terminal, digit `$ git remote add upstream $ENDERECO_QUE_VOCE_ACABOU_COPIAR.`

### Crie duas branches em seu repsoitório origin

1. `development`: mantém sincronia com a branch development do upstream e é utilizada para novas implementações;
2. `master`: mantém sincronia com a master do repo upstream;

### Diariamente realize o comando git fetch upstream

Em seguida, faça o checkout para a branch master e digite git merge upstream/master. Isso fará um merge com as últimas atualizaçòes do upstream no seu ambiente local de desenvolvimento. Também faça o checkout para seu branch development, git merge upstream/development. Isso deixará sua branch atualizada com a branch de produção.

## Ambiente de Desenvolvimento

---

1. Se você não tem instalado ainda, roda a instalação global do Gulp: `npm install -g gulp`;
2. Para rodar seu ambiente de dev instale as devependências com `npm install`;
3. Após tudo instalado, rode o comando `gulp` ou `npm start`;

## Ambiente de Deploy

---

1. Para fazer deploy o pipeline vai buildar os arquivos estáticos e colocar na pasta build separados por HTML;
2. O comando `gulp build` gera os arquivos estáticos concatenando os HTMLs;
3. Os arquivos a serem entregues no projeto estão dentro da pasta build;
